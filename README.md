# Où est Billy

This is my last school project. For more information (in French), please refer to the [Medium article](https://medium.com/@robindevouge/tfe-o%C3%B9-est-billy-ec5ee6867106).

# heroku CLI commands
/note: the following is there as a personal reminder to use the corresponding repo on heroku, it's not necessary if you run the node server locally.

## scale up :
heroku ps:scale web=1

## scale down :
heroku ps:scale web=0

## scale status :
heroku ps

## deploy version (master only):
git push heroku master

## read logs
heroku logs --tail
