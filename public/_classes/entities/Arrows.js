var arrows = new THREE.Group();
var arrow1 = new THREE.Group();

var arrowMat = new THREE.MeshLambertMaterial({
	color:0xffffff,
	transparent: true
});

var y = 7;
var displacement = 170;

var b1 = new THREE.Mesh(createBox(y,y,y),arrowMat);
var b2 = new THREE.Mesh(createBox(3*y,y,y),arrowMat);
var b3 = new THREE.Mesh(createBox(5*y,y,y),arrowMat);
var b4 = new THREE.Mesh(createBox(7*y,y,y),arrowMat);
var b5 = new THREE.Mesh(createBox(9*y,y,y),arrowMat);
var b6 = new THREE.Mesh(createBox(11*y,y,y),arrowMat);
var b7 = new THREE.Mesh(createBox(13*y,y,y),arrowMat);
var b8 = new THREE.Mesh(createBox(15*y,y,y),arrowMat);
var b9 = new THREE.Mesh(createBox(17*y,y,y),arrowMat);

b1.position.set(0,0,displacement+8*y+y/2);
b2.position.set(0,0,displacement+7*y+y/2);
b3.position.set(0,0,displacement+6*y+y/2);
b4.position.set(0,0,displacement+5*y+y/2);
b5.position.set(0,0,displacement+4*y+y/2);
b6.position.set(0,0,displacement+3*y+y/2);
b7.position.set(0,0,displacement+2*y+y/2);
b8.position.set(0,0,displacement+1*y+y/2);
b9.position.set(0,0,displacement+0*y+y/2);

arrow1.add(b1,b2,b3,b4,b5,b6,b7,b8,b9);


var arrow2 = arrow1.clone();
var arrow3 = arrow1.clone();
var arrow4 = arrow1.clone();

arrow2.rotation.set(0,toRad(90),0);
arrow3.rotation.set(0,toRad(180),0);
arrow4.rotation.set(0,toRad(270),0);


arrow1.name = "SW";
arrow2.name = "SE";
arrow3.name = "NE";
arrow4.name = "NW";

arrows.add(arrow1,arrow2,arrow3,arrow4);
arrows.position.set(0,-15+y/2,0);

arrowMat.opacity = 0;
// console.log(arrows);
