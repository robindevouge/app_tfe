Tree = function(){
	this.x = Math.floor(Math.random() * world.bounds.x2 * 2 - world.bounds.x2);
	this.z = Math.floor(Math.random() * world.bounds.z2 * 2 - world.bounds.z2);

	this.angle = toRad(getRandom(90));

	this.mesh = new THREE.Group;

	this.trunk = new THREE.Mesh(createBox(20,100,20),new THREE.MeshLambertMaterial({
		color: 0x3e2723
	}))
	this.trunk.position.set(0,50,0);

	this.foliage1 = new THREE.Mesh(createCube(100),new THREE.MeshLambertMaterial({
		color: 0x66bb6a
	}))
	this.foliage2 = new THREE.Mesh(createCube(80),new THREE.MeshLambertMaterial({
		color: 0x66bb6a
	}))
	this.foliage3 = new THREE.Mesh(createCube(60),new THREE.MeshLambertMaterial({
		color: 0x66bb6a
	}))
	this.foliage1.position.set(-15,150,10);
	this.foliage2.position.set(20,180,-10);
	this.foliage3.position.set(-10,210,10);

	this.mesh.rotation.set(0,this.angle,0);
	this.mesh.position.set(this.x,0,this.z);

	this.mesh.add(this.trunk);
	this.mesh.add(this.foliage1);
	this.mesh.add(this.foliage2);
	this.mesh.add(this.foliage3);

	this.mesh.castShadow = true;
	this.mesh.children.map(function(obj){
		obj.castShadow = true;
		obj.receiveShadow = true;
	})
}

Grass = function(){
	this.x = Math.floor(Math.random() * world.bounds.x2 * 2 - world.bounds.x2);
	this.z = Math.floor(Math.random() * world.bounds.z2 * 2 - world.bounds.z2);

	this.angle = toRad(getRandom(90));

	this.mesh = new THREE.Group;
	this.blade1 = new THREE.Mesh(createBox(3,10,3),new THREE.MeshLambertMaterial({
		color: 0x388e3c
	}))
	this.blade2 = new THREE.Mesh(createBox(3,15,3),new THREE.MeshLambertMaterial({
		color: 0x388e3c
	}))
	this.blade3 = new THREE.Mesh(createBox(3,6,3),new THREE.MeshLambertMaterial({
		color: 0x388e3c
	}))

	this.blade1.position.set(10,5,0);
	this.blade2.position.set(-5,7.5,5);
	this.blade3.position.set(0,3,-10);

	this.mesh.add(this.blade1);
	this.mesh.add(this.blade2);
	this.mesh.add(this.blade3);

	this.mesh.rotation.set(0,this.angle,0);
	this.mesh.position.set(this.x,0,this.z);
	this.mesh.castShadow = true;
	this.mesh.children.map(function(obj){
		obj.castShadow = true;
		obj.receiveShadow = true;
	})
}
