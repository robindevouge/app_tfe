Pawn = function() {

	this.materials = {};

	this.refs = {
		'colors': {
			'skin': getRandom(globalMaterials.skin.length),
			'shirt': getRandom(globalMaterials.basic.length),
			'hair': getRandom(globalMaterials.skin.length)
		},
		'hairStyle': getRandom(hairStyles.basic.length),
		'hasBackpack': false
	}
	// placed after cuz no self ref, y'know
	this.refs.colors.pants = getOtherRandom(this.refs.colors.shirt, globalMaterials.basic.length);
	this.refs.colors.shoes = getOtherRandom(this.refs.colors.pants, globalMaterials.basic.length);
	this.refs.colors.backpack = getOtherRandom(this.refs.colors.shirt, globalMaterials.basic.length);


	this.setMaterials();
	this.createMeshComponents();
	this.setHairStyle();
	this.setPositions();

	// shadows
	this.mesh.castShadow = true;
	this.mesh.children.map(function(obj) {
		obj.castShadow = true;
		obj.receiveShadow = true;
	})

	// world data objects

	this.data = {
		'name': 'pawn',
		'position': {
			'x': Math.floor(Math.random() * world.bounds.x2 * 2 - world.bounds.x2),
			'y': 0,
			'z': Math.floor(Math.random() * world.bounds.z2 * 2 - world.bounds.z2)
		},
		'angle': toRad(getRandom(360)),
		'speed': Math.random() + 0.5,
		'height': Math.random() * (0.35 - 0.25) + 0.25,
		'timings': {
			'step': 0,
			'moving': getRandomBetween(60, 320),
			'idle': getRandomBetween(10, 50),
			'isIdle': false
		}
	}
	this.data.footstep = {
		'speed': 15 * 1 / this.data.speed,
		'step': 0
	};

	this.mesh.position.set(this.data.position.x, this.data.position.y, this.data.position.z);
	this.mesh.scale.set(.3, this.data.height, .3);
	this.mesh.rotation.set(0, this.data.angle, 0);
}

// METHODS

// creation

Pawn.prototype.createMeshComponents = function() {
	this.mesh = new THREE.Group;
	this.body = {
		'head': new THREE.Group,
		'face': new THREE.Mesh(createCube(50), this.materials.skin),
		'hair': [],
		'torso': new THREE.Mesh(createBox(50, 80, 25), this.materials.shirt),
		'arms': {
			'left': new THREE.Group,
			'right': new THREE.Group
		},
		'forearms': {
			'left': new THREE.Mesh(createBox(25, 90, 25), this.materials.shirt),
			'right': new THREE.Mesh(createBox(25, 90, 25), this.materials.shirt)
		},
		'hands': {
			'left': new THREE.Mesh(createBox(25, 15, 25), this.materials.skin),
			'right': new THREE.Mesh(createBox(25, 15, 25), this.materials.skin)
		},
		'legs': new THREE.Mesh(createBox(50, 110, 25), this.materials.pants),
		'feet': {
			'static': new THREE.Mesh(createBox(51, 25, 26), this.materials.shoes),
			'moving': new THREE.Mesh(createBox(26, 25, 20), this.materials.shoes),
			'pos': -13
		}
	}
	// adding to groups
	this.body.head.add(this.body.face);
	this.body.arms.left.add(this.body.forearms.left);
	this.body.arms.right.add(this.body.forearms.right);
	this.body.arms.left.add(this.body.hands.left);
	this.body.arms.right.add(this.body.hands.right);
	// adding to global mesh
	this.mesh.add(this.body.head);
	this.mesh.add(this.body.torso);
	this.mesh.add(this.body.arms.left);
	this.mesh.add(this.body.arms.right);
	this.mesh.add(this.body.legs);
	this.mesh.add(this.body.feet.moving);
	this.mesh.add(this.body.feet.static);
	if(this.refs.hasBackpack) {
		//create
		this.body.backpack = new THREE.Group;
		this.body.packParts = {
			'pack': new THREE.Mesh(createBox(55, 70, 20), this.materials.backpack),
			'pouch': new THREE.Mesh(createBox(45, 30, 10), this.materials.backpack),
			'lanyards': {
				'left': new THREE.Mesh(createBox(10, 70, 35), globalMaterials.hair[5]),
				'right': new THREE.Mesh(createBox(10, 70, 35), globalMaterials.hair[5])
			}
		}
		//add
		this.body.backpack.add(this.body.packParts.pack);
		this.body.backpack.add(this.body.packParts.pouch);
		this.body.backpack.add(this.body.packParts.lanyards.left);
		this.body.backpack.add(this.body.packParts.lanyards.right);
		this.mesh.add(this.body.backpack);
	}
};

Pawn.prototype.setMaterials = function() {
	this.materials.skin = globalMaterials.skin[this.refs.colors.skin];
	this.materials.shirt = globalMaterials.basic[this.refs.colors.shirt];
	this.materials.pants = globalMaterials.basic[this.refs.colors.pants];
	this.materials.shoes = globalMaterials.basic[this.refs.colors.shoes];

	if(this.refs.colors.hair == 0) { //flash hair
		this.refs.colors.hair = getRandom(globalMaterials.flashHair.length);
		this.materials.hair = globalMaterials.flashHair[this.refs.colors.hair];
	} else {
		this.materials.hair = globalMaterials.hair[this.refs.colors.hair];
	}

	if(this.refs.hasBackpack) {
		this.materials.backpack = globalMaterials.basic[this.refs.colors.backpack];
	}
}

Pawn.prototype.setHairStyle = function() {
	if(this.refs.hairStyle == 0) { //special hair
		this.refs.hairStyle = getRandom(hairStyles.special.length);
		this.generateHair(hairStyles.special, hairStyles.posSpecial)
	} else {
		this.generateHair(hairStyles.basic, hairStyles.posBasic);
	}
}

Pawn.prototype.generateHair = function(hairMesh, hairPos) {
	for(var i = 0; i < hairMesh[this.refs.hairStyle].length; i++) {
		this.body.hair[i] = new THREE.Mesh(hairMesh[this.refs.hairStyle][i], this.materials.hair);
		this.body.hair[i].position.set(hairPos[this.refs.hairStyle][i][0], hairPos[this.refs.hairStyle][i][1], hairPos[this.refs.hairStyle][i][2]);
		this.body.head.add(this.body.hair[i]);
	}
}

Pawn.prototype.setPositions = function() {
	this.body.head.position.set(0, 215, 0);
	this.body.torso.position.set(0, 150, 0);
	this.body.arms.left.position.set(37.5, 190, 0);
	this.body.arms.right.position.set(-37.5, 190, 0);
	this.body.forearms.left.position.set(0, -45, 0);
	this.body.forearms.right.position.set(0, -45, 0);
	this.body.hands.left.position.set(0, -97.5, 0);
	this.body.hands.right.position.set(0, -97.5, 0);
	this.body.legs.position.set(0, 55, 0);
	this.body.feet.static.position.set(0, 12.5, 0);
	this.body.feet.moving.position.set(this.body.feet.pos, 12.5, 22.5);
	if(this.refs.hasBackpack) {
		this.body.packParts.lanyards.left.position.set(-22, 25, 22.5);
		this.body.packParts.lanyards.right.position.set(22, 25, 22.5);
		this.body.packParts.pouch.position.set(0, -15, -15);
		this.body.backpack.position.set(0, 135, -22.5);
	}
};

// functioning

Pawn.prototype.walk = function() {
	if(this.data.timings.step < this.data.timings.moving) {
		this.data.timings.step++;

		this.data.position.x += this.data.speed * Math.sin(this.data.angle);
		this.data.position.z += this.data.speed * Math.cos(this.data.angle);
		if(this.data.position.x < world.bounds.x1) {
			this.data.position.x = world.bounds.x2
		}
		if(this.data.position.x > world.bounds.x2) {
			this.data.position.x = world.bounds.x1
		}
		if(this.data.position.z < world.bounds.z1) {
			this.data.position.z = world.bounds.z2
		}
		if(this.data.position.z > world.bounds.z2) {
			this.data.position.z = world.bounds.z1
		}
		this.mesh.position.x = this.data.position.x;
		this.mesh.position.z = this.data.position.z;
	} else {
		if(this.data.timings.step < this.data.timings.moving + this.data.timings.idle) {
			this.data.timings.step++;
			this.data.timings.isIdle = true;
		} else {
			this.data.angle = toRad(getRandom(360));
			this.mesh.rotation.set(0, this.data.angle, 0);
			this.data.timings.step = 0;
			this.data.timings.moving = getRandomBetween(60, 320);
			this.data.timings.idle = getRandomBetween(10, 50);
			this.data.timings.isIdle = false;
		}
	}
}

Pawn.prototype.animateFeet = function() {
	if(this.data.footstep.step < this.data.footstep.speed) {
		this.data.footstep.step++;
	} else {
		this.body.feet.pos = -this.body.feet.pos;
		this.data.footstep.step = 0;
	}
	if(!this.data.timings.isIdle) {
		this.body.feet.moving.position.x = this.body.feet.pos;
	}
}
