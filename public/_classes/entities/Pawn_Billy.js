function Pawn_Billy(){
	Pawn.call(this);

	this.data.name = 'billy';

	this.refs = {
		'colors': {
			'skin': 0,
			'shirt': 1,
			'pants':4,
			'shoes':0,
			'hair': 1
		},
		'hairStyle': 1,
		'hasBackpack': true
	}
	this.refs.colors.backpack = getOtherRandom(this.refs.colors.shirt, globalMaterials.basic.length);

	// this.body = {};
	this.setMaterials();
	this.createMeshComponents();
	this.setHairStyle();
	this.setPositions();

	this.mesh.position.set(this.data.position.x, this.data.position.y, this.data.position.z);
	this.mesh.scale.set(.3, this.data.height, .3);
	this.mesh.rotation.set(0, this.data.angle, 0);
}
Pawn_Billy.prototype = Object.create(Pawn.prototype);
Pawn_Billy.prototype.constructor = Pawn_Billy;

Pawn_Billy.prototype.walk = function() {
	if(this.data.timings.step < this.data.timings.moving) {
		this.data.timings.step++;

		this.data.position.x += this.data.speed * Math.sin(this.data.angle);
		this.data.position.z += this.data.speed * Math.cos(this.data.angle);
		if(this.data.position.x < world.bounds.x1) {
			this.data.angle = toRad(getRandomBetween(0,180));
			this.mesh.rotation.set(0, this.data.angle, 0);
		}
		if(this.data.position.x > world.bounds.x2) {
			this.data.angle = toRad(getRandomBetween(180,360));
			this.mesh.rotation.set(0, this.data.angle, 0);
		}
		if(this.data.position.z < world.bounds.z1) {
			this.data.angle = toRad(getRandomBetween(-90,90));
			this.mesh.rotation.set(0, this.data.angle, 0);
		}
		if(this.data.position.z > world.bounds.z2) {
			this.data.angle = toRad(getRandomBetween(90,270));
			this.mesh.rotation.set(0, this.data.angle, 0);
		}
		this.mesh.position.x = this.data.position.x;
		this.mesh.position.z = this.data.position.z;
	} else {
		if(this.data.timings.step < this.data.timings.moving + this.data.timings.idle) {
			this.data.timings.step++;
			this.data.timings.isIdle = true;
		} else {
			this.data.angle = toRad(getRandom(360));
			this.mesh.rotation.set(0, this.data.angle, 0);
			this.data.timings.step = 0;
			this.data.timings.moving = getRandomBetween(60, 320);
			this.data.timings.idle = getRandomBetween(10, 50);
			this.data.timings.isIdle = false;
		}
	}
}
