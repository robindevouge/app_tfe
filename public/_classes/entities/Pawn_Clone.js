function Pawn_Clone(data){
	Pawn.call(this, data);

	this.data = data;

	this.refs = {
		'colors': {
			'skin': this.data.refs.colors.skin,
			'shirt': this.data.refs.colors.shirt,
			'pants':this.data.refs.colors.pants,
			'shoes':this.data.refs.colors.shoes,
			'hair': this.data.refs.colors.hair
		},
		'hairStyle': this.data.refs.hairStyle,
		'hasBackpack': this.data.refs.hasBackpack
	}
	this.refs.colors.backpack = this.data.refs.colors.backpack;

	// this.body = {};
	this.setMaterials();
	this.createMeshComponents();
	this.setHairStyle();
	this.setPositions();

	this.mesh.position.set(this.data.position.x, this.data.position.y, this.data.position.z);
	this.mesh.scale.set(.3, this.data.height, .3);
	this.mesh.rotation.set(0, this.data.angle, 0);
	this.mesh.name = this.data.name;
}
Pawn_Clone.prototype = Object.create(Pawn.prototype);
Pawn_Clone.prototype.constructor = Pawn_Clone;

Pawn_Clone.prototype.getIsInBounds = function(pCoord) {
	// console.log(pCoord);
	if(pCoord.x < world.pawnSpawnBounds.x1 || pCoord.x > world.pawnSpawnBounds.x2 || pCoord.z < world.pawnSpawnBounds.z1 || pCoord.z > world.pawnSpawnBounds.z2) {
		return false;
	} else {
		return true;
	}
}

Pawn_Clone.prototype.enteredBounds = function() {
	if(this.isInBounds == false && this.isInBounds != this.wasInBounds){
		return true;
	}else{
		return false;
	}
}

Pawn_Clone.prototype.leftBounds = function() {
	if(this.isInBounds == true && this.isInBounds != this.wasInBounds){
		return true;
	}else{
		return false;
	}
}

Pawn_Clone.prototype.update = function(pData) {
	this.isInBounds = this.getIsInBounds(pData.position);
	if(this.isInBounds) {
		this.setPosition(pData.position);
		this.setRotation(pData.angle);
		if(this.enteredBounds){this.setVisibility(true)}
	} else {
		if(this.leftBounds){this.setVisibility(false)}
	}
	this.wasInBounds = this.isInBounds
}

Pawn_Clone.prototype.setPosition = function(pCoord) {
	this.mesh.position.set(pCoord.x,0,pCoord.z);
}

Pawn_Clone.prototype.setRotation = function(pAngle) {
	this.mesh.rotation.y = pAngle;
}

Pawn_Clone.prototype.setVisibility = function(val) {
	this.mesh.children.map(function(obj) {
		obj.visible = val;
	})
};

// Pawn_Clone.prototype.animateFeet = function() {
// 	if(this.footstepStage < this.footstepSpeed) {
// 		this.footstepStage++;
// 	} else {
// 		this.body.feet.pos = -this.body.feet.pos;
// 		this.footstepStage = 0;
// 	}
// 	this.body.feet.moving.position.x = this.body.feet.pos;
// }
