var globalMaterials = {
	"skin": [
		//pale
		new THREE.MeshLambertMaterial({
			color: 0xfce4ec
		}),
		//black
		new THREE.MeshLambertMaterial({
			color: 0x4e342e
		}),
		//arabic
		new THREE.MeshLambertMaterial({
			color: 0xa1887f
		}),
		//asian
		new THREE.MeshLambertMaterial({
			color: 0xffcc80
		}),
		//tanned
		new THREE.MeshLambertMaterial({
			color: 0xbcaaa4
		}),
	],
	"hair": [
		// if 0 then hair color is one of flash hair color > reduced chances to have flash colors
		"go to flashHair",
		//brown
		new THREE.MeshLambertMaterial({
			color: 0x795548
		}),
		//venitian
		new THREE.MeshLambertMaterial({
			color: 0x7f522d
		}),
		//blond
		new THREE.MeshLambertMaterial({
			color: 0xc5b147
		}),
		//lightblond
		new THREE.MeshLambertMaterial({
			color: 0xffee58
		}),
		//black
		new THREE.MeshLambertMaterial({
			color: 0x212121
		}),
		//lightgrey
		new THREE.MeshLambertMaterial({
			color: 0xbdbdbd
		}),
		//grey
		new THREE.MeshLambertMaterial({
			color: 0x757575
		})
		//mine
		// new THREE.MeshLambertMaterial({
		// 	color: 0x978f3d
		// })
	],
	"flashHair": [
		//carrot
		new THREE.MeshLambertMaterial({
			color: 0xff9800
		}),
		//blue
		new THREE.MeshLambertMaterial({
			color: 0x2962ff
		}),
		//red
		new THREE.MeshLambertMaterial({
			color: 0xd50000
		}),
		//pink
		new THREE.MeshLambertMaterial({
			color: 0xf06292
		})
	],
	"basic": [
		//white
		new THREE.MeshLambertMaterial({
			color: 0xffffff
		}),
		//red
		new THREE.MeshLambertMaterial({
			color: 0xef5350
		}),
		//orange
		new THREE.MeshLambertMaterial({
			color: 0xffa726
		}),
		//blue
		new THREE.MeshLambertMaterial({
			color: 0x2196f
		}),
		//lightblue
		new THREE.MeshLambertMaterial({
			color: 0x2196f3
		}),
		//grey
		new THREE.MeshLambertMaterial({
			color: 0x424242
		}),
		//pink
		new THREE.MeshLambertMaterial({
			color: 0xec407a
		}),
		//green
		new THREE.MeshLambertMaterial({
			color: 0x388e3c
		}),
		//purple
		new THREE.MeshLambertMaterial({
			color: 0x9c27b0
		}),
		//yellow
		new THREE.MeshLambertMaterial({
			color: 0xfdd835
		}),
	]
}

var hairStyles = {
	"basic": [
		[ //leads to special hairstyle
		],
		[ //basic
			new THREE.BoxBufferGeometry(54, 2, 54),
			new THREE.BoxBufferGeometry(54, 40, 2),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(54, 10, 2)
		],
		[ //ponytail
			new THREE.BoxBufferGeometry(54, 2, 54),
			new THREE.BoxBufferGeometry(54, 50, 2),
			new THREE.BoxBufferGeometry(2, 30, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(2, 30, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(54, 10, 2),
			new THREE.BoxBufferGeometry(13, 80, 13)
		],
		[ //richie
			new THREE.BoxBufferGeometry(54, 2, 54),
			new THREE.BoxBufferGeometry(54, 40, 2),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
		],
		[ //long
			new THREE.BoxBufferGeometry(54, 2, 54),
			new THREE.BoxBufferGeometry(54, 100, 2),
			new THREE.BoxBufferGeometry(2, 40, 54),
			new THREE.BoxBufferGeometry(2, 80, 10),
			new THREE.BoxBufferGeometry(2, 40, 54),
			new THREE.BoxBufferGeometry(2, 80, 10),
			new THREE.BoxBufferGeometry(54, 10, 2)
		],
		[ // afro
			new THREE.BoxBufferGeometry(70,40,70),
			new THREE.BoxBufferGeometry(70,40,30)
		]
	],
	"special": [
		[ //tonsure
			new THREE.BoxBufferGeometry(54, 30, 2),
			new THREE.BoxBufferGeometry(10, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(10, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 20),
			new THREE.BoxBufferGeometry(54, 10, 20)
		],
		[ //punk
			new THREE.BoxBufferGeometry(20, 50, 50),
			new THREE.BoxBufferGeometry(10, 15, 10)
		],
		[ //bald
		],
		[ //mulet
			new THREE.BoxBufferGeometry(54, 2, 54),
			new THREE.BoxBufferGeometry(54, 70, 10),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 10),
			new THREE.BoxBufferGeometry(2, 20, 54),
			new THREE.BoxBufferGeometry(2, 20, 10),
			new THREE.BoxBufferGeometry(54, 20, 2)
		]
	],
	"posBasic": [
		[ //leads to special hairstyle
		],
		[ //basic
			[0, 26, 0],
			[0, 5, -26],
			[26, 15, 0],
			[26, -5, -16],
			[-26, 15, 0],
			[-26, -5, -16],
			[0, 20, 26]
		],
		[ //ponytail
			[0, 26, 0],
			[0, 0, -26],
			[26, 10, 0],
			[26, -15, -16],
			[-26, 10, 0],
			[-26, -15, -16],
			[0, 20, 26],
			[0, -30, -33.5]
		],
		[ //richie
			[0, 26, 0],
			[0, 5, -26],
			[26, 15, 0],
			[26, -5, -16],
			[-26, 15, 0],
			[-26, -5, -16],
		],
		[ //long
			[0, 26, 0],
			[0, -25, -26],
			[26, 5, 0],
			[26, -35, -20],
			[-26, 5, 0],
			[-26, -35, -20],
			[0, 20, 26]
		],
		[ //afro
			[0,25,0],
			[0,5,-20]
		]
	],
	"posSpecial": [
		[ //tonsure
			[0, 2, -26],
			[22, 17, 0],
			[26, -3, -16],
			[-22, 17, 0],
			[-22, -3, -16],
			[0, 22, 17]
		],
		[ //punk
			[0, 10, -10],
			[0, -20, 22]
		],
		[ //bald
		],
		[ //mulet
			[0, 26, 0],
			[0, -10, -22],
			[26, 15, 0],
			[26, -5, 5],
			[-26, 15, 0],
			[-26, -5, 5],
			[0, 15, 26]
		]
	]
}
