Demo = function() {
	this.lights = [];
	this.container = document.querySelector('.ui__demo');

	this.initScene();
	this.initRenderer();
	this.initCamera();
	this.initLight();

	this.render();

}

// PROTOTYPES

Demo.prototype.initScene = function() {
	this.width = this.container.offsetWidth;
	this.height = this.container.offsetHeight;

	this.scene = new THREE.Scene();

	window.scene = this.scene;
	window.THREE = THREE;
}

Demo.prototype.initRenderer = function() {
	this.renderer = new THREE.WebGLRenderer({
		antialias: true,
		alpha: true
	});
	this.renderer.setSize(this.width, this.height);
	this.renderer.shadowMap.enabled = true;
	this.renderer.shadowMap.bias = 0.0039;
	this.renderer.setClearColor(0x29b6f6, 0);
	// this.renderer.setClearColor(0xffffff);
	this.container.appendChild(this.renderer.domElement);
}

Demo.prototype.initCamera = function() {
	this.fieldOfView = 60;
	this.aspectRatio = this.width / this.height;
	this.nearPlane = -500;
	this.farPlane = 2000;

	const left = this.width / -2;
	const right = this.width / 2;
	const top = this.height / -2;
	const bottom = this.height / 2;

	this.camera = new THREE.OrthographicCamera(
		left,
		right,
		bottom,
		top,
		this.nearPlane,
		this.farPlane
	);
	this.camPosX = 500;
	this.camPosY = 400;
	this.camPosZ = 500;
	this.camera.position.set(this.camPosX, this.camPosY, this.camPosZ);
	this.camera.lookAt(new THREE.Vector3(0, 120, 0));
}

Demo.prototype.initLight = function() {
	const ambientLight = new THREE.AmbientLight(0xfffde7, 0.6, 0, 1);
	ambientLight.position.set(0, 800, 0);
	this.scene.add(ambientLight);

	const mainLight = new THREE.DirectionalLight(0xfffde7, 0.3);
	mainLight.position.set(-200, 500, 500);
	mainLight.castShadow = true;
	mainLight.shadow.mapSize.width = 1024;
	mainLight.shadow.mapSize.height = 1024;
	mainLight.shadow.camera.near = 1;
	mainLight.shadow.camera.far = 1500;
	mainLight.shadow.camera.top = 800;
	mainLight.shadow.camera.bottom = -800;
	mainLight.shadow.camera.right = 800;
	mainLight.shadow.camera.left = -800;
	this.lights.push(mainLight);
	this.scene.add(mainLight);

	const backLight = new THREE.SpotLight(0xffffff, 0.2, 0, 1);
	backLight.position.set(700, 300, -800);
	backLight.castShadow = false;
	this.lights.push(backLight);
	this.scene.add(backLight);
}

// RENDER

Demo.prototype.render = function() {
	this.renderer.render(this.scene, this.camera);

	if(this.billy) {
		this.billy.angle += 1;
		this.billy.rotation.set(0, toRad(this.billy.angle), 0);
	}

	requestAnimationFrame(function() {
		this.render();
	}.bind(this));

}

// CUSTOM

Demo.prototype.addEntity = function(ent) {
	this.billy = ent;
	this.scene.add(ent);

	this.billy.position.set(0, 0, 0);
	this.billy.scale.set(1, 1, 1);
	this.billy.rotation.set(0, 0, 0);
	this.billy.angle = 0;
	this.billy.children.map(function(obj) {
		obj.castShadow = false;
		obj.receiveShadow = false;
	})
}
