SearchZone = function() {
	this.lights = [];
	this.entities = [];
	this.lightEntities = [];
	this.deco = [];

	this.bounds = {
		"x1": -500,
		"x2": 500,
		"z1": -500,
		"z2": 500
	}

	this.initScene();
	this.initRenderer();
	this.initCamera();
	this.initLight();
	this.initFloor();
	this.initGrid();
	this.initText();

	if(debug.controls) {
		this.addOrbitControl();
	}
	if(debug.helpers) {
		this.addHelpers();
	}

	fpsInterval = 1000 / fpsMax;
	then = Date.now();
	startTime = then;
	fpsLimiter(this);
	// this.render();
}

// PROTOTYPES

SearchZone.prototype.initScene = function() {
	this.width = canvas.offsetWidth;
	this.height = canvas.offsetHeight;

	this.scene = new THREE.Scene();

	window.scene = this.scene;
	window.THREE = THREE;
}

SearchZone.prototype.initRenderer = function() {
	this.renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	this.renderer.setSize(this.width, this.height);
	this.renderer.shadowMap.enabled = true;
	this.renderer.shadowMap.bias = 0.0039;
	this.renderer.setClearColor(0x29b6f6);
	canvas.appendChild(this.renderer.domElement);
}

SearchZone.prototype.initCamera = function() {
	this.fieldOfView = 60;
	this.aspectRatio = this.width / this.height;
	this.nearPlane = -500;
	this.farPlane = 2000;

	this.displacementH = 0;
	this.displacementV = 50;

	this.diagH = 750;
	this.diagV = 560;
	this.verticalLimit = this.diagH / this.aspectRatio;
	if(this.verticalLimit < this.diagV) {
		var left = -this.diagV * this.aspectRatio + this.displacementH;
		var right = this.diagV * this.aspectRatio + this.displacementH;
		var top = -this.diagV + this.displacementV;
		var bottom = this.diagV + this.displacementV;
	} else {
		var left = -this.diagH + this.displacementH;
		var right = this.diagH + this.displacementH;
		var top = -this.diagH / this.aspectRatio + this.displacementV;
		var bottom = this.diagH / this.aspectRatio + this.displacementV;
	}

	this.camera = new THREE.OrthographicCamera(
		left,
		right,
		bottom,
		top,
		this.nearPlane,
		this.farPlane
	);
	this.camPosX = 500;
	this.camPosY = 400;
	this.camPosZ = 500;
	this.camera.position.set(this.camPosX, this.camPosY, this.camPosZ);
	this.camera.lookAt(new THREE.Vector3(0, 0, 0));
}

SearchZone.prototype.initLight = function() {
	const ambientLight = new THREE.AmbientLight(0xfffde7, 0.6, 0, 1);
	ambientLight.position.set(0, 800, 0);
	this.scene.add(ambientLight);

	const mainLight = new THREE.DirectionalLight(0xfffde7, 0.3);
	mainLight.position.set(-200, 500, 500);
	mainLight.castShadow = true;
	mainLight.shadow.mapSize.width = 1024;
	mainLight.shadow.mapSize.height = 1024;
	mainLight.shadow.camera.near = 1;
	mainLight.shadow.camera.far = 1500;
	mainLight.shadow.camera.top = 800;
	mainLight.shadow.camera.bottom = -800;
	mainLight.shadow.camera.right = 800;
	mainLight.shadow.camera.left = -800;
	this.lights.push(mainLight);
	this.scene.add(mainLight);

	const backLight = new THREE.SpotLight(0xffffff, 0.2, 0, 1);
	backLight.position.set(700, 300, -800);
	backLight.castShadow = false;
	this.lights.push(backLight);
	this.scene.add(backLight);
}

SearchZone.prototype.initFloor = function() {
	const floor = new THREE.Mesh(new THREE.BoxBufferGeometry(995, 50, 995), new THREE.MeshLambertMaterial({
		color: 0x4caf50
	}));
	floor.position.set(0, -(25 + 5), 0);
	this.scene.add(floor);
}

SearchZone.prototype.initGrid = function() {
	this.grid = new THREE.Group;
	this.tileGeo = createBox(195, 5, 195);
	this.tileMat = new THREE.MeshLambertMaterial({
		color: 0x4caf50
	});
	this.tiles = {
		"elements": [],
		"positions": [
			[-400, -2.5, 400], //A1
			[-200, -2.5, 400], //A2
			[0, -2.5, 400], //A3
			[200, -2.5, 400], //A4
			[400, -2.5, 400], //A5
			[-400, -2.5, 200], //B1
			[-200, -2.5, 200], //B2
			[0, -2.5, 200], //B3
			[200, -2.5, 200], //B4
			[400, -2.5, 200], //B5
			[-400, -2.5, 0], //C1
			[-200, -2.5, 0], //C2
			[0, -2.5, 0], //C3
			[200, -2.5, 0], //C4
			[400, -2.5, 0], //C5
			[-400, -2.5, -200], //D1
			[-200, -2.5, -200], //D2
			[0, -2.5, -200], //D3
			[200, -2.5, -200], //D4
			[400, -2.5, -200], //D5
			[-400, -2.5, -400], //E1
			[-200, -2.5, -400], //E2
			[0, -2.5, -400], //E3
			[200, -2.5, -400], //E4
			[400, -2.5, -400] //E5
		]
	}

	for(var i = 0; i < 25; i++) {
		this.tiles.elements.push(new THREE.Mesh(this.tileGeo, this.tileMat));
		this.tiles.elements[i].position.set(this.tiles.positions[i][0], this.tiles.positions[i][1], this.tiles.positions[i][2]);
		this.grid.add(this.tiles.elements[i]);
	}

	this.grid.children.map(function(obj) {
		obj.receiveShadow = true;
	})

	this.scene.add(this.grid)
}

SearchZone.prototype.initText = function() {

	var loader = new THREE.FontLoader();

	var text = [
	{
		'value': 'A',
		'position': {
			'x': -400,
			'y': -45,
			'z': 500
		}
	},
	{
		'value': 'B',
		'position': {
			'x': -200,
			'y': -45,
			'z': 500
		}
	},
	{
		'value': 'C',
		'position': {
			'x': 0,
			'y': -45,
			'z': 500
		}
	},
	{
		'value': 'D',
		'position': {
			'x': 200,
			'y': -45,
			'z': 500
		}
	},
	{
		'value': 'E',
		'position': {
			'x': 400,
			'y': -45,
			'z': 500
		}
	},
	{
		'value': '1',
		'position': {
			'x': 500,
			'y': -45,
			'z': 400
		}
	},
	{
		'value': '2',
		'position': {
			'x': 500,
			'y': -45,
			'z': 200
		}
	},
	{
		'value': '3',
		'position': {
			'x': 500,
			'y': -45,
			'z': 0
		}
	},
	{
		'value': '4',
		'position': {
			'x': 500,
			'y': -45,
			'z': -200
		}
	},
	{
		'value': '5',
		'position': {
			'x': 500,
			'y': -45,
			'z': -400
		}
	}
	];

	loader.load('./_scripts/Open_Sans_Regular.json', function(font) {
		var textMat = new THREE.MeshLambertMaterial({
			color: 0xffffff
		});

		for(var i = 0; i < text.length; i++) {
			var textGeo = new THREE.TextGeometry(text[i].value, {
				font: font,
				size: 40,
				height: 3,
				curveSegments: 2
			});

			textGeo.computeBoundingBox()
			var textWidth = textGeo.boundingBox.max.x;

			var textMesh = new THREE.Mesh(textGeo, textMat);

			if(text[i].value == 'A' || text[i].value == 'B' || text[i].value == 'C' || text[i].value == 'D' || text[i].value == 'E'){
				textMesh.position.set(text[i].position.x - textWidth/2, text[i].position.y, text[i].position.z);
			}else{
				textMesh.position.set(text[i].position.x, text[i].position.y, text[i].position.z + textWidth/2);
				textMesh.rotation.set(0,toRad(90),0);
			}

			world.scene.add(textMesh);
		}
	});
}
// RENDER

SearchZone.prototype.render = function() {
	// rendering is very slow causing timeouts more than 50ms
	// const startTime = performance.now();
	this.renderer.render(this.scene, this.camera);
	// const duration = performance.now() - startTime;
	// console.log(`someMethodIThinkMightBeSlow took ${duration}ms`);

	var datas = [];

	for(var i = 0; i < this.entities.length; i++) {
		this.entities[i].walk();
		this.entities[i].animateFeet();
		// this.entities[i].changeDir();
		var data = {
			'position': this.entities[i].data.position,
			'angle': this.entities[i].data.angle,
			'isIdle': this.entities[i].data.timings.isIdle
		}
		datas.push(data);
	}
	// console.log(datas);
	socket.emit('screen_updatePawn', datas);

	// requestAnimationFrame(function() {
	// 	this.render();
	// }.bind(this));

}

// CUSTOM

SearchZone.prototype.addEntity = function(ent) {
	this.entities.push(ent);
	this.scene.add(ent.mesh);
}

SearchZone.prototype.getEntityData = function(id) {
	var datas = [];
	for(var i = 0; i < this.entities.length; i++) {
		var data = this.entities[i].data;
		data.refs = this.entities[i].refs;
		data.id = id;
		datas.push(data);
	}
	return datas;
}

SearchZone.prototype.addDeco = function(ent) {
	this.deco.push(ent);
	this.scene.add(ent.mesh);
}
