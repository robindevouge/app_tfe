Studio = function() {
	this.lights = [];
	this.entities = [];

	this.bounds = {
		"x1": -50,
		"x2": 400,
		"z1": -400,
		"z2": 500
	}

	this.initScene();
	this.initRenderer();
	this.initCamera();
	this.initLight();

	this.addOrbitControl();

	this.render();
}

// PROTOTYPES

Studio.prototype.initScene = function() {
	this.width = window.innerWidth;
	this.height = window.innerHeight;

	this.scene = new THREE.Scene();

	window.scene = this.scene;
	window.THREE = THREE;
}

Studio.prototype.initRenderer = function() {
	this.renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	this.renderer.setSize(this.width, this.height);
	this.renderer.shadowMap.enabled = true;
	this.renderer.shadowMap.bias = 0.0039;
	// this.renderer.setClearColor(0xaa00ff);
	// this.renderer.setClearColor(0x4caf50);
	this.renderer.setClearColor(0xffffff);
	document.body.appendChild(this.renderer.domElement);
}

Studio.prototype.initCamera = function() {
	const fieldOfView = 60;
	const aspectRatio = this.width / this.height;
	const nearPlane = 1;
	const farPlane = 2000;

	const left = this.width / -2;
	const right = this.width / 2;
	const top = this.height / -2;
	const bottom = this.height / 2;
	const near = -500;
	const far = 2000;

	this.camera = new THREE.OrthographicCamera(
		left,
		right,
		bottom,
		top,
		near,
		far
	);
	this.camPosX = 500;
	this.camPosY = 400;
	this.camPosZ = 500;
	this.camera.position.set(this.camPosX, this.camPosY, this.camPosZ);
	this.camera.lookAt(new THREE.Vector3(0, 0, 0));
}

Studio.prototype.initLight = function() {
	const ambientLight = new THREE.AmbientLight(0xffffff, 0.7, 0, 1);
	ambientLight.position.set(0, 800, 0);
	this.scene.add(ambientLight);

	const mainLight = new THREE.DirectionalLight(0xffffff, 0.1);
	mainLight.position.set(-50, 500, 50);
	mainLight.castShadow = true;
	mainLight.shadow.mapSize.width = 4096;
	mainLight.shadow.mapSize.height = 4096;
	mainLight.shadow.camera.near = 1;
	mainLight.shadow.camera.far = 1500;
	mainLight.shadow.camera.top = 800;
	mainLight.shadow.camera.bottom = -800;
	mainLight.shadow.camera.right = 800;
	mainLight.shadow.camera.left = -800;
	this.lights.push(mainLight);
	this.scene.add(mainLight);

	const backLight = new THREE.SpotLight(0xffffff, 0.2, 0, 1);
	backLight.position.set(700, 300, -800);
	backLight.castShadow = false;
	this.lights.push(backLight);
	this.scene.add(backLight);
}

Studio.prototype.addOrbitControl = function() {
	this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement)
}

// RENDER

Studio.prototype.render = function() {
	this.renderer.render(this.scene, this.camera);
	this.controls.update();

	requestAnimationFrame(function() {
		this.render();
	}.bind(this));

}

// CUSTOM

Studio.prototype.addEntity = function(ent) {
	this.entities.push(ent);
	this.scene.add(ent.mesh);
}
