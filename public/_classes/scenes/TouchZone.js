TouchZone = function() {
	this.lights = [];
	this.entities = [];
	this.entitiesMeshes = [];

	this.ui = {
		'all': document.querySelector(".ui"),
		'back': ui__back,
		'coord': document.querySelector(".ui__coord"),
		'explain': document.querySelector(".ui__explain")
	}
	// this.lightEntities = [];
	// this.deco = [];
	this.zoomedIn = false;

	this.bounds = {
		"x1": -500,
		"x2": 500,
		"z1": -500,
		"z2": 500
	}
	this.positions = {
		"obj": {
			"A1": [-400, -50, 400],
			"A2": [-400, -50, 200],
			"A3": [-400, -50, 0],
			"A4": [-400, -50, -200],
			"A5": [-400, -50, -400],
			"B1": [-200, -50, 400],
			"B2": [-200, -50, 200],
			"B3": [-200, -50, 0],
			"B4": [-200, -50, -200],
			"B5": [-200, -50, -400],
			"C1": [0, -50, 400],
			"C2": [0, -50, 200],
			"C3": [0, -50, 0],
			"C4": [0, -50, -200],
			"C5": [0, -50, -400],
			"D1": [200, -50, 400],
			"D2": [200, -50, 200],
			"D3": [200, -50, 0],
			"D4": [200, -50, -200],
			"D5": [200, -50, -400],
			"E1": [400, -50, 400],
			"E2": [400, -50, 200],
			"E3": [400, -50, 0],
			"E4": [400, -50, -200],
			"E5": [400, -50, -400]
		},
		"array": [
			[-400, -50, 400],
			[-400, -50, 200],
			[-400, -50, 0],
			[-400, -50, -200],
			[-400, -50, -400],
			[-200, -50, 400],
			[-200, -50, 200],
			[-200, -50, 0],
			[-200, -50, -200],
			[-200, -50, -400],
			[0, -50, 400],
			[0, -50, 200],
			[0, -50, 0],
			[0, -50, -200],
			[0, -50, -400],
			[200, -50, 400],
			[200, -50, 200],
			[200, -50, 0],
			[200, -50, -200],
			[200, -50, -400],
			[400, -50, 400],
			[400, -50, 200],
			[400, -50, 0],
			[400, -50, -200],
			[400, -50, -400]
		]
	}

	this.currentLocation = 0;
	this.names = ["A1", "A2", "A3", "A4", "A5", "B1", "B2", "B3", "B4", "B5", "C1", "C2", "C3", "C4", "C5", "D1", "D2", "D3", "D4", "D5", "E1", "E2", "E3", "E4", "E5"];
	this.tiles = [];

	this.animTiming = {
		"short": 0.3,
		"medium": 0.5,
		"long": 0.7
	}

	this.cellOpacity = {
		'min': 0.2,
		'max': 1
	}

	this.initScene();
	this.initRenderer();
	this.initCamera();
	this.initLight();
	// this.addOrbitControl();
	this.initGrid();

	this.arrows = arrows;
	this.arrowMat = arrowMat;
	this.scene.add(arrows);
}

// PROTOTYPES

TouchZone.prototype.initScene = function() {
	this.width = canvasContainer.offsetWidth;
	this.height = canvasContainer.offsetHeight;

	this.scene = new THREE.Scene();

	window.scene = this.scene;
	window.THREE = THREE;
}

TouchZone.prototype.initRenderer = function() {
	this.renderer = new THREE.WebGLRenderer({
		antialias: true
	});
	this.renderer.setSize(this.width, this.height);
	this.renderer.shadowMap.enabled = true;
	this.renderer.shadowMap.bias = 0.0039;
	this.renderer.setClearColor(0x29b6f6);
	canvasContainer.appendChild(this.renderer.domElement);
}

TouchZone.prototype.initCamera = function() {
	this.aspectRatio = this.width / this.height;
	this.nearPlane = 1;
	this.farPlane = 2000;

	this.displacementH = 0;
	this.displacementV = 50;

	this.diagH = 750;
	this.diagV = 560;
	this.verticalLimit = this.diagH / this.aspectRatio;
	if(this.verticalLimit < this.diagV) {
		this.left = -this.diagV * this.aspectRatio + this.displacementH;
		this.right = this.diagV * this.aspectRatio + this.displacementH;
		this.top = -this.diagV + this.displacementV;
		this.bottom = this.diagV + this.displacementV;
	} else {
		this.left = -this.diagH + this.displacementH;
		this.right = this.diagH + this.displacementH;
		this.top = -this.diagH / this.aspectRatio + this.displacementV;
		this.bottom = this.diagH / this.aspectRatio + this.displacementV;
	}

	this.camera = new THREE.OrthographicCamera(
		this.left,
		this.right,
		this.bottom,
		this.top,
		this.nearPlane,
		this.farPlane
	);
	this.camPosX = 500;
	this.camPosY = 400;
	this.camPosZ = 500;
	this.camera.position.set(this.camPosX, this.camPosY, this.camPosZ);
	this.camera.lookAt(new THREE.Vector3(0, 0, 0));

	this.raycaster = new THREE.Raycaster(); // create once
	this.mouse = new THREE.Vector2(); // create once
}

TouchZone.prototype.initLight = function() {
	const ambientLight = new THREE.AmbientLight(0xfffde7, 0.6, 0, 1);
	ambientLight.position.set(0, 800, 0);
	this.scene.add(ambientLight);

	const mainLight = new THREE.DirectionalLight(0xfffde7, 0.3);
	mainLight.position.set(-200, 500, 500);
	mainLight.castShadow = true;
	mainLight.shadow.mapSize.width = 1024;
	mainLight.shadow.mapSize.height = 1024;
	mainLight.shadow.camera.near = 1;
	mainLight.shadow.camera.far = 1500;
	mainLight.shadow.camera.top = 800;
	mainLight.shadow.camera.bottom = -800;
	mainLight.shadow.camera.right = 800;
	mainLight.shadow.camera.left = -800;
	this.lights.push(mainLight);
	this.scene.add(mainLight);

	const backLight = new THREE.SpotLight(0xffffff, 0.2, 0, 1);
	backLight.position.set(700, 300, -800);
	backLight.castShadow = false;
	this.lights.push(backLight);
	this.scene.add(backLight);
}

TouchZone.prototype.initGrid = function() {
	this.grid = new THREE.Group;
	this.tileGeo = createBox(195, 50, 195);

	for(var i = 0; i < 25; i++) {
		this.tiles.push(new THREE.Mesh(this.tileGeo, new THREE.MeshLambertMaterial({
			color: 0x4caf50,
			transparent: true
		})));
		this.tiles[i].name = this.names[i];
		this.tiles[i].position.set(this.positions.array[i][0], this.positions.array[i][1], this.positions.array[i][2]);
		this.grid.add(this.tiles[i]);
	}

	this.grid.children.map(function(obj) {
		obj.receiveShadow = true;
	})

	this.scene.add(this.grid);
}

TouchZone.prototype.setAuthDir = function(loc) {
	this.authDirections = [true, true, true, true];

	if((loc) % 5 == 0) {
		this.authDirections[0] = false;
	}
	if(loc + 5 >= this.names.length) {
		this.authDirections[1] = false;
	}
	if((loc + 1) % 5 == 0) {
		this.authDirections[2] = false;
	}
	if(loc - 5 < 0) {
		this.authDirections[3] = false;
	}

	for(var i = 0; i < this.authDirections.length; i++) {
		this.arrows.children[i].visible = this.authDirections[i]
	}
}

TouchZone.prototype.setLocation = function(type, loc, arr) {
	switch(type) {
		case 'cell':
			this.currentLocation = loc;
			this.setAuthDir(loc);
			break;
		case 'arrow':
			var from = loc,
				to = 0;
			switch(arr) {
				case 'SW':
					to = from - 1;
					this.setAuthDir(from);
					if(this.authDirections[0]) {
						this.currentLocation = to;
					} else {
						this.currentLocation = from;
					}
					break;
				case 'SE':
					to = from + 5;
					this.setAuthDir(from);
					if(this.authDirections[1]) {
						this.currentLocation = to;
					} else {
						this.currentLocation = from;
					}
					break;
				case 'NE':
					to = from + 1;
					this.setAuthDir(from);
					if(this.authDirections[2]) {
						this.currentLocation = to;
					} else {
						this.currentLocation = from;
					}
					break;
				case 'NW':
					to = from - 5;
					this.setAuthDir(from);
					if(this.authDirections[3]) {
						this.currentLocation = to;
					} else {
						this.currentLocation = from;
					}
					break;
			}
			this.setAuthDir(this.currentLocation)
			break;
	}
	this.zoomOnCell(this.currentLocation);
};

TouchZone.prototype.setRaycaster = function(evt){
	this.mouse.x = (evt.clientX / this.width) * 2 - 1;
	this.mouse.y = -((evt.clientY-54) / this.height) * 2 + 1;
	this.raycaster.setFromCamera(this.mouse, this.camera);
}

TouchZone.prototype.cellTap = function(evt) {
	this.setRaycaster(evt);
	var intersects = this.raycaster.intersectObjects(this.tiles);
	if(intersects.length > 0) {
		if(!this.zoomedIn) {
			this.startUpdateListener();
		}
		this.setLocation('cell', this.names.indexOf(intersects[0].object.name));
	}
};

TouchZone.prototype.pawnTap = function(evt) {
	this.setRaycaster(evt);
	var intersects = this.raycaster.intersectObjects(this.entitiesMeshes, true);
	if(intersects.length > 0) {
		var selection = intersects[0].object
		while(selection.name != "pawn" && selection.name != "billy") {
			selection = selection.parent;
		}
		if(selection.name == "billy") {
			socket.emit('player_foundBilly');
			fadeIn(screen_foundBilly);
			this.stopUpdateListener();
		}
	}
};
TouchZone.prototype.arrowTap = function(evt) {
	this.setRaycaster(evt);
	var intersects = this.raycaster.intersectObjects(this.arrows.children, true);
	if(intersects.length > 0) {
		var selection = intersects[0].object
		while(selection.name != "NE" && selection.name != "NW" && selection.name != "SE" && selection.name != "SW") {
			selection = selection.parent;
		}
		this.setLocation('arrow', this.currentLocation, selection.name)
	}
};

TouchZone.prototype.zoomOnCell = function(coord) {
	// if orbit is on, play with that :
	//	this.controls.target.set(this.selectedTile[0], this.selectedTile[1], this.selectedTile[2]);

	var index = coord;
	this.selectedTile = this.tiles[index];

	// move + zoom camera
	TweenLite.to(this.camera.position, this.animTiming.medium, {
		x: this.selectedTile.position.x + this.camPosX,
		y: -25 + this.camPosY,
		z: this.selectedTile.position.z + this.camPosZ
	});
	TweenLite.to(this.camera, this.animTiming.medium, {
		zoom: 4,
		delay: this.animTiming.medium - 0.1,
		onUpdate: function() {
			this.camera.updateProjectionMatrix();
		}.bind(this)
	});
	TweenLite.to(this.arrows.position, this.animTiming.short, {
		x: this.selectedTile.position.x,
		y: -15 + y / 2,
		z: this.selectedTile.position.z
	})
	if(!this.zoomedIn) {
		for(var i = 0; i < this.tiles.length; i++) {
			TweenLite.to(this.tiles[i].material, this.animTiming.short, {
				opacity: this.cellOpacity.min
			})
		}
		TweenLite.to(this.arrowMat, this.animTiming.medium, {
			opacity: 1,
			delay: this.animTiming.medium
		})
		fadeIn(this.ui.coord);
		fadeIn(this.ui.back);
		fadeOut(this.ui.explain);
	}

	// move selected tile up
	TweenLite.to(this.selectedTile.position, this.animTiming.short, {
		y: -25
	});
	TweenLite.to(this.selectedTile.material, this.animTiming.short, {
		opacity: this.cellOpacity.max
	})

	// move other tiles down
	for(var i = 0; i < this.tiles.length; i++) {
		// only if it was up AND not the same
		if(this.tiles[i].position.y != this.positions.array[i][1] && this.selectedTile != this.tiles[i]) {
			TweenLite.to(this.tiles[i].position, this.animTiming.short, {
				y: this.positions.array[i][1]
			});
			TweenLite.to(this.tiles[i].material, this.animTiming.short, {
				opacity: this.cellOpacity.min
			})
		}
	}

	// set spawn bounds
	this.pawnSpawnBounds = {
		"x1": this.selectedTile.position.x - 100,
		"x2": this.selectedTile.position.x + 100,
		"z1": this.selectedTile.position.z - 100,
		"z2": this.selectedTile.position.z + 100
	}
	//
	this.zoomedIn = true;

	this.ui.coord.innerHTML = this.names[this.currentLocation];
};

TouchZone.prototype.zoomOut = function() {
	this.zoomedIn = false;
	// zoom out
	TweenLite.to(this.camera, this.animTiming.medium, {
		zoom: 1,
		onUpdate: function() {
			this.camera.updateProjectionMatrix();
		}.bind(this)
	});
	// move to the center
	TweenLite.to(this.camera.position, this.animTiming.medium, {
		x: this.camPosX,
		y: this.camPosY,
		z: this.camPosZ,
		delay: this.animTiming.medium
	})
	// tiles down
	for(var i = 0; i < this.tiles.length; i++) {
		if(this.tiles[i].position.y != this.positions.array[i][1]) {
			TweenLite.to(this.tiles[i].position, this.animTiming.short, {
				y: this.positions.array[i][1]
			});
		}
		TweenLite.to(this.tiles[i].material, this.animTiming.short, {
			opacity: this.cellOpacity.max
		})
	}
	//remove arrows
	TweenLite.to(this.arrowMat, this.animTiming.medium, {
		opacity: 0
		// delay: this.animTiming.medium
	})
	fadeOut(this.ui.coord);
	fadeOut(this.ui.back);
	fadeIn(this.ui.explain);
}

TouchZone.prototype.addOrbitControl = function() {
	this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
	this.controls.minPolarAngle = toRad(51); // radians
	this.controls.maxPolarAngle = toRad(51); // radians
	this.controls.minZoom = 1;
	this.controls.maxZoom = 2;
	this.controls.zoomSpeed = 0.4;
	this.controls.enablePan = false;
}

// RENDER

TouchZone.prototype.render = function() {
	this.renderer.render(this.scene, this.camera);
	// this.controls.update();

	for(var i = 0; i < this.entities.length; i++) {
		if(!this.entities[i].isIdle) {
			this.entities[i].animateFeet();
		}
	}

	this.loop = requestAnimationFrame(function() {
		this.render();
	}.bind(this));

}

TouchZone.prototype.updatePawnLoop = function(data) {
	this.pawnData = data;
	for(var i = 0; i < this.entities.length; i++) {
		this.entities[i].update(this.pawnData[i]);
	}
}

TouchZone.prototype.startUpdateListener = function() {
	socket.on('server_updatePawn', this.updatePawnLoop.bind(this));
}
TouchZone.prototype.stopUpdateListener = function() {
	socket.off('server_updatePawn');
	for(var i = 0; i < world.entities.length; i++) {
		world.entities[i].setVisibility(false);
	}
}

// CUSTOM

TouchZone.prototype.addEntity = function(ent) {
	ent.mesh.children.map(function(obj) {
		obj.visible = false;
	});
	this.entities.push(ent);
	this.entitiesMeshes.push(ent.mesh)
	this.scene.add(ent.mesh);
}

TouchZone.prototype.getEntityData = function(id) {
	var datas = [];
	for(var i = 0; i < this.entities.length; i++) {
		var data = {
			'id': id,
			'coord': this.entities[i].coord,
			'angle': this.entities[i].angle,
			'colorRef': this.entities[i].colorRef,
			'hairRef': this.entities[i].body.hair.ref,
			'hairStatus': this.entities[i].body.hair.special,
			'height': this.entities[i].height,
			'speed': this.entities[i].speed
		};
		datas.push(data);
	}
	return datas;
}

TouchZone.prototype.addDeco = function(ent) {
	this.deco.push(ent);
	this.scene.add(ent.mesh);
}
