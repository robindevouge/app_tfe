function toRad(ang) {
	var res = ang * Math.PI / 180;
	return res;
}

function toDeg(ang) {
	var res = ang * 180 / Math.PI;
	return res;
}

function createCube(size) {
	return new THREE.BoxGeometry(size, size, size);
}

function createBox(x, y, z) {
	return new THREE.BoxGeometry(x, y, z);
}

function getRandom(limit) {
	return Math.floor(Math.random() * limit);
}

function getRandomBetween(min,max) {
	return Math.floor(Math.random()*(max-min+1)+min);
}

function getOtherRandom(testValue, length) {
	// debugger
	var testA = testValue;
	var testB = getRandom(length);

	if(testA === testB) {
		return getOtherRandom(testA, length);
	} else {
		return testB;
	}
}

// from this example : http://jsfiddle.net/m1erickson/CtsY3/
var stop = false;
var frameCount = 0;
var fpsMax = 25;
var fpsInterval, startTime, now, then, elapsed;
function fpsLimiter(obj) {

    // request another frame

    requestAnimationFrame(fpsLimiter);

    // calc elapsed time since last loop

    now = Date.now();
    elapsed = now - then;

    // if enough time has elapsed, draw the next frame

    if (elapsed > fpsInterval) {

        // Get ready for next frame by setting then=now, but also adjust for your
        // specified fpsInterval not being a multiple of RAF's interval (16.7ms)
        then = now - (elapsed % fpsInterval);

        // Put your drawing code here
		world.render()

    }
}

// GRAPHIC

function fadeIn(el){
	el.classList.remove('hidden');
	setTimeout(function(){
		el.classList.remove('transparent');
	},1)

};
function fadeOut(el){
	el.classList.add('transparent');
	setTimeout(function(){
		el.classList.add('hidden');
	},300);
};
