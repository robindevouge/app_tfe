console.log('player script loaded');

var title = document.querySelector('.title'),
	screen_canvas = document.querySelector('.screen--canvas'),
	canvasContainer = document.querySelector('.canvasContainer'),
	ui__back = document.querySelector('.ui__back'),
	remaining = document.querySelector('.info__remaining'),
	screen_noGame = document.querySelector('.screen--noGame'),
	screen_foundBilly = document.querySelector('.screen--foundBilly'),
	screen_allWin = document.querySelector('.screen--allWin');

var world = new TouchZone();

window.addEventListener('touchend', function(event) {
	if(!world.zoomedIn) {
		world.cellTap(event.changedTouches[0]);
	} else {
		world.pawnTap(event.changedTouches[0]);
		world.arrowTap(event.changedTouches[0]);
	}
});

// zoom out on back press
ui__back.addEventListener('touchend', function() {
	world.zoomOut();
	world.stopUpdateListener();
});

// socket listeners

var entities = [];

socket.on('server_start', function(data) {
	entities = data;

	world.render();

	for(var i = 0; i < entities.length; i++) {
		var pawn = new Pawn_Clone(entities[i]);
		world.addEntity(pawn)
	}

	fadeOut(screen_noGame);
})

socket.on('server_someoneFoundBilly', function(data){
	// console.log('left : '+data);
	remaining.innerHTML = data;
	// update the variable to display how many players still have to find it
})
socket.on('server_allWin', function(){
	console.log('all win');
	fadeIn(screen_allWin);
	// display the final screen
})

socket.on('server_notPlaying', function(){
	console.log('not playing');
	fadeIn(screen_noGame);
	// display a screen where it tells the game has not yet started
})

socket.on('server_screenOn', function(){
	console.log('screen on');
	socket.emit('player_connect', socket.id);
})

// START

socket.emit('player_connect', socket.id);
