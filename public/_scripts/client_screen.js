console.log('screen script loaded');

socket.emit('screen_connect');

var screen_allWin = document.querySelector('.screen--win'),
	canvas = document.querySelector('.canvas'),
	ui = document.querySelector('.ui');

var debug = {
	"controls": false,
	"helpers": false,
	"animations": true,
	"modeling": false,
	"painting": false
}

var world = new SearchZone();

for(var i = 0; i < 200; i++) {
	var pawn = new Pawn();
	world.addEntity(pawn);
}

var billy = new Pawn_Billy();
var demoBilly = billy.mesh.clone();
world.addEntity(billy);

for(var i = 0; i < 8; i++) {
	var el = new Tree();
	world.addDeco(el)
}

for(var i = 0; i < 20; i++) {
	var el = new Grass();
	world.addDeco(el)
}

var demo = new Demo();
demo.addEntity(demoBilly);

socket.on('server_requestEntities', function(id) {
	socket.emit('screen_sendEntities', world.getEntityData(id));
});

socket.on('server_allWin', function(){
	ui.classList.add('expand');
	fadeIn(screen_allWin);
});
