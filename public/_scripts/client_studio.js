var debug = {
	"controls": true,
	"helpers": false,
	"animations": true,
	"modeling": false,
	"painting": false
}

var world = new Studio();

var park = {
	"bounds": {
		"x1": -0,
		"x2": 0,
		"z1": -0,
		"z2": 0
	}
}


// box

var box = new THREE.Mesh(createBox(200,40,100),new THREE.MeshLambertMaterial({color: 0x4a4a4a}))
var antenna = new THREE.Mesh(createBox(10,75,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}))
var antenna2 = new THREE.Mesh(createBox(10,75,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}))
var light = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x66bb6a}))
var light2 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x66bb6a}))
var light3 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x66bb6a}))
var wave = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave2 = new THREE.Mesh(createBox(30,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave2_1 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave2_2 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave3 = new THREE.Mesh(createBox(50,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave3_1 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave3_2 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave3_3 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))
var wave3_4 = new THREE.Mesh(createBox(10,10,1),new THREE.MeshLambertMaterial({color: 0x2196f3}))


antenna.position.set(90,30,-55)
antenna2.position.set(-90,30,-55)
light.position.set(-80,0,50)
light2.position.set(-60,0,50)
light3.position.set(-40,0,50)

var wifi = new THREE.Group();
wifi.add(wave,wave2,wave2_1,wave2_2,wave3,wave3_1,wave3_2,wave3_3,wave3_4);
wifi.position.set(0,40,-50);
wifi.scale.set(1.3,1.3,1.3)

wave.position.set(0,0,0)
wave2.position.set(0,30,0)
wave2_1.position.set(-20,20,0)
wave2_2.position.set(20,20,0)
wave3.position.set(0,60,0)
wave3_1.position.set(-40,40,0)
wave3_2.position.set(40,40,0)
wave3_3.position.set(-30,50,0)
wave3_4.position.set(30,50,0)

var modem = new THREE.Group();
modem.add(box,antenna,antenna2,light,light2,light3,wifi);

world.scene.add(modem);
modem.position.set(-250,105,-150);
modem.scale.set(.3,.3,.3);
modem.rotation.set(0,toRad(90),0);

// stand

var stand = new THREE.Group();
var standBottom = new THREE.Mesh(createBox(75,100,200),new THREE.MeshLambertMaterial({color: 0xffffff}));
stand.add(standBottom);
stand.position.set(-250,50,-100)
world.scene.add(stand);


// screen

var tv = new THREE.Group();
var topTV = new THREE.Mesh(createBox(160,5,4),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var bottomTV = new THREE.Mesh(createBox(160,5,4),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var leftTV = new THREE.Mesh(createBox(5,90,4),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var rightTV = new THREE.Mesh(createBox(5,90,4),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var midTV = new THREE.Mesh(createBox(150,80,1),new THREE.MeshLambertMaterial({color: 0x64dd17}));
var support = new THREE.Mesh(createBox(10,100,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var base = new THREE.Mesh(createBox(50,3,30),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));

topTV.position.set(0,42.5,0);
bottomTV.position.set(0,-42.5,0);
leftTV.position.set(-77.5,0,0);
rightTV.position.set(77.5,0,0);
support.position.set(0,-50,-4);
base.position.set(0,-100,0);

tv.add(topTV,bottomTV,leftTV,rightTV,midTV,support,base);
world.scene.add(tv);

tv.position.set(100,208,-400);
tv.scale.set(2,2,2);

// hours

var hours = new THREE.Group();
var hoursPanel = new THREE.Mesh(createBox(5,260,600),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var hoursSupport1 = new THREE.Mesh(createBox(5,100,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var hoursSupport2 = new THREE.Mesh(createBox(5,100,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var sPart1 = new THREE.Mesh(createBox(5,10,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var sPart2 = new THREE.Mesh(createBox(5,5,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var sPart3 = new THREE.Mesh(createBox(5,10,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var sPart4 = new THREE.Mesh(createBox(5,5,5),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
hours.add(hoursPanel, hoursSupport1, hoursSupport2, sPart1, sPart2, sPart3, sPart4);

hoursSupport1.position.set(0,150,100);
hoursSupport2.position.set(0,150,-100);
sPart1.position.set(0,210,100);
sPart2.position.set(0,222,100);
sPart3.position.set(0,210,-100);
sPart4.position.set(0,222,-100);
hours.position.set(-500,400,0);
world.scene.add(hours);



// phone
/*
var phone = new THREE.Group();

var topPhone = new THREE.Mesh(createBox(110,20,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var bottomPhone = new THREE.Mesh(createBox(110,30,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var leftPhone = new THREE.Mesh(createBox(10,160,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var rightPhone = new THREE.Mesh(createBox(10,160,10),new THREE.MeshLambertMaterial({color: 0x4a4a4a}));
var midPhone = new THREE.Mesh(createBox(90,160,9),new THREE.MeshLambertMaterial({color: 0x64dd17}));

topPhone.position.set(0,87.5,0);
bottomPhone.position.set(0,-95,0);
leftPhone.position.set(-50,0,0);
rightPhone.position.set(50,0,0);

var touch1 = new THREE.Group();
var touch2 = new THREE.Group();

var t1 = new THREE.Mesh(createBox(5,20,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t2 = new THREE.Mesh(createBox(20,5,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t3 = new THREE.Mesh(createBox(5,20,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t4 = new THREE.Mesh(createBox(20,5,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t5 = new THREE.Mesh(createBox(5,20,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t6 = new THREE.Mesh(createBox(20,5,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t7 = new THREE.Mesh(createBox(5,20,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));
var t8 = new THREE.Mesh(createBox(20,5,1),new THREE.MeshLambertMaterial({color: 0x2196f3}));

touch1.position.set(-25,30,5.5);
touch2.position.set(-25,30,5.5);
touch2.rotation.set(0,0,toRad(45));
t1.position.set(0,30,0);
t2.position.set(30,0,0);
t3.position.set(0,-30,0);
t4.position.set(-30,0,0);
t5.position.set(0,30,0);
t6.position.set(30,0,0);
t7.position.set(0,-30,0);
t8.position.set(-30,0,0);


phone.add(topPhone,bottomPhone,leftPhone,rightPhone,midPhone);
touch1.add(t1,t2,t3,t4);
touch2.add(t5,t6,t7,t8);
world.scene.add(phone);
world.scene.add(touch1);
world.scene.add(touch2);

*/

/*
var lego = new THREE.Group();

var block = new THREE.Mesh(createBox(40, 20, 40), new THREE.MeshLambertMaterial({
	color: 0xef5350
}));
var b1 = new THREE.Mesh(new THREE.CylinderGeometry(6, 6, 5, 32), new THREE.MeshLambertMaterial({
	color: 0xff5350
}));
var b2 = new THREE.Mesh(new THREE.CylinderGeometry(6, 6, 5, 32), new THREE.MeshLambertMaterial({
	color: 0xff5350
}));
var b3 = new THREE.Mesh(new THREE.CylinderGeometry(6, 6, 5, 32), new THREE.MeshLambertMaterial({
	color: 0xff5350
}));
var b4 = new THREE.Mesh(new THREE.CylinderGeometry(6, 6, 5, 32), new THREE.MeshLambertMaterial({
	color: 0xff5350
}));

b1.position.set(-10, 12.5, -10);
b2.position.set(-10, 12.5, 10);
b3.position.set(10, 12.5, -10);
b4.position.set(10, 12.5, 10);

lego.add(block, b1, b2, b3, b4);
world.scene.add(lego);


var arrows = new THREE.Group();
var arrow1 = new THREE.Group();

var arrowMat = new THREE.MeshLambertMaterial({color:0xffffff});

var y = 10;
var displacement = 150;

var b1 = new THREE.Mesh(createBox(y,y,y),arrowMat);
var b2 = new THREE.Mesh(createBox(3*y,y,y),arrowMat);
var b3 = new THREE.Mesh(createBox(5*y,y,y),arrowMat);
var b4 = new THREE.Mesh(createBox(7*y,y,y),arrowMat);
var b5 = new THREE.Mesh(createBox(9*y,y,y),arrowMat);
var b6 = new THREE.Mesh(createBox(11*y,y,y),arrowMat);
var b7 = new THREE.Mesh(createBox(13*y,y,y),arrowMat);
var b8 = new THREE.Mesh(createBox(15*y,y,y),arrowMat);
var b9 = new THREE.Mesh(createBox(17*y,y,y),arrowMat);

b1.position.set(0,0,displacement+8*y+y/2);
b2.position.set(0,0,displacement+7*y+y/2);
b3.position.set(0,0,displacement+6*y+y/2);
b4.position.set(0,0,displacement+5*y+y/2);
b5.position.set(0,0,displacement+4*y+y/2);
b6.position.set(0,0,displacement+3*y+y/2);
b7.position.set(0,0,displacement+2*y+y/2);
b8.position.set(0,0,displacement+1*y+y/2);
b9.position.set(0,0,displacement+0*y+y/2);

arrow1.add(b1,b2,b3,b4,b5,b6,b7,b8,b9);


var arrow2 = arrow1.clone();
var arrow3 = arrow1.clone();
var arrow4 = arrow1.clone();

arrow2.rotation.set(0,toRad(90),0);
arrow3.rotation.set(0,toRad(180),0);
arrow4.rotation.set(0,toRad(270),0);

arrows.add(arrow1,arrow2,arrow3,arrow4)
world.scene.add(arrows);

var cell = new THREE.Mesh(createBox(200,50,200), new THREE.MeshLambertMaterial({color:0x4caf50}));
world.scene.add(cell)

*/

var floor = new THREE.Mesh(createBox(1000,10,1000), new THREE.MeshLambertMaterial({color:0xe0e0e0}));
floor.receiveShadow = true;
world.scene.add(floor);

var mobs = [];
for (var i = 0; i < 13; i++) {
	mobs[i] = new Pawn();
	world.addEntity(mobs[i]);
	mobs[i].mesh.scale.set(.8,.8,.8);
	mobs[i].mesh.rotation.set(0,toRad(180),0);
}

mobs[0].mesh.rotation.set(0,toRad(90),0);
mobs[0].mesh.position.set(-320,5,-100);
mobs[1].mesh.position.set(0,5,-100);
mobs[2].mesh.position.set(40,5,100);
mobs[3].mesh.position.set(200,5,67);
mobs[4].mesh.position.set(300,5,-200);
mobs[5].mesh.position.set(120,5,-150);
mobs[6].mesh.position.set(360,5,-50);
mobs[7].mesh.position.set(-150,5,200);
mobs[8].mesh.position.set(-50,5,250);
mobs[9].mesh.position.set(420,5,400);
mobs[10].mesh.position.set(320,5,350);
mobs[11].mesh.position.set(340,5,220);
mobs[12].mesh.position.set(250,5,300);

// var mob = new Pawn();
// var mob = new Billy();


// world.addEntity(mob);
// mob.mesh.position.set(0, -250, 0);
// mob.mesh.rotation.set(0, toRad(180), 0);
// mob.mesh.scale.set(2,2,2);

for (var i = 0; i < world.scene.children.length; i++) {
	world.scene.children[i].children.map(function(obj) {
		obj.castShadow = true;
	})
}
