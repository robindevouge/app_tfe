var path = require('path');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var os = require('os');

var interfaces = os.networkInterfaces();
var addresses = [];
for(var k in interfaces) {
	for(var k2 in interfaces[k]) {
		var address = interfaces[k][k2];
		if(address.family === 'IPv4' && !address.internal) {
			addresses.push(address.address);
		}
	}
}

app.set('port', (process.env.PORT || 5000));

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/public/index.html');
});
app.use(express.static(__dirname + '/public'));

console.info('Started application at ' + addresses[0] + ':' + app.settings.port);

// VARS INIT

var players = {
	'total':0,
	'percentage':0,
	'win':0,
	'left':0
}

var playing = false;

var screenID = "";

// METHODS

function resetGame() {
	players = {
		'total':0,
		'percentage':0,
		'win':0,
		'left':0
	}
	// client.emit('server_notPlaying');
	console.info('Game has been reset');
}

function updatePlayers(){
	players.percentage = Math.ceil(90/100*players.total);
	players.left = players.percentage - players.win;
	console.log("players: "+players.total+" || %: "+players.percentage+" || left: "+players.left);
}

function updateWin(){
	players.left = players.percentage - players.win;
	if (players.left <= 0) {
		players.left = 0;
		playing = false;
		io.emit('server_allWin');
		console.log('all win');
		// find a way to reset everyone including server
		resetGame();
	}else{
		io.emit('server_someoneFoundBilly', players.left);
		console.log('left: '+players.left);
	}
}

// SOCKET EVENTS

io.sockets.on('connection', function(client) {

	// PLAYER EVENTS

	client.on('player_connect', function(){
		if (playing) {
			players.total++;
			updatePlayers();
			io.emit('server_requestEntities', client.id);
		}else{
			client.emit('server_notPlaying');
		}

	});

	client.on('player_foundBilly', function(){
		players.win++;
		updateWin();
	});

	// SCREEN EVENTS

	client.on('screen_connect', function(){
		screenID = (client.id);
		playing = true;
		client.broadcast.emit('server_screenOn');
		console.log("screen connected with id "+screenID);
	});

	client.on('screen_sendEntities', function(data) {
		client.broadcast.to(data[0].id).emit('server_start', data);
	});
	client.on('screen_updatePawn', function(data) {
		client.broadcast.emit('server_updatePawn', data);
	});

	// CLIENT DISCONNECT
	client.on('disconnect', function() {
		if (client.id != screenID) {
			players.total--;
			if (players.total < 0) {
				players.total = 0;
			}
			console.log("player disconnected");
			updatePlayers();
		}
		else{
			playing = false;
			console.log("screen disconnected");
			resetGame();
		}
	});
});



http.listen(app.get('port'));
